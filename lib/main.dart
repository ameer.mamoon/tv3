// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'API/views.dart';
import 'UI/screens.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static TextEditingController controller = TextEditingController();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        'movies': (context) => HomeScreen(TvWidget.getTop250moviesWidgets(), 'Top Movies'),
        'tvs': (context) => HomeScreen(TvWidget.getTop250tvsWidgets(),'Top TVs'),

      },
      home: HomeScreen(TvWidget.getTop250moviesWidgets(),'Top TVs'),


    );
  }
}
