// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:tv/API/views.dart';
import 'package:tv/UI/elements.dart';

class HomeScreen extends StatelessWidget {

  final Future future;
  final String title;

  HomeScreen(this.future,this.title);

  static final TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SearchWidget('Home : $title',textController),
        centerTitle: ! SearchController.controller.isSearch,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              if(textController.text.isNotEmpty){
                String value = textController.text;
                textController.text = '';
                Navigator.push(context, MaterialPageRoute(builder: (context) => SearchScreen(value)));
              }

                SearchController.controller.press();
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: future,
        builder: (context,asyncSnapshot){
          if(asyncSnapshot.hasData){
            List<Widget> children = asyncSnapshot.data as List<Widget>;
            return SingleChildScrollView(
              child: Wrap(
                children: children,
              ),
            );

          }

          if(asyncSnapshot.hasError){
            return Center(child: Text('No Internet Connection!'));
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
                padding: EdgeInsets.zero,
                child: Container(
                  color: Theme.of(context).primaryColor,
                )
            ),
            Card(
              child: ListTile(
                title: Text('Top Movies'),
                subtitle: Text('Top 250 Movies in the world'),
                leading: Icon(Icons.movie_filter_outlined),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () => Navigator.pushReplacementNamed(context, 'movies')
              ),
              
            ),
            Card(
              child: ListTile(
                title:    Text('Top TVs'),
                subtitle: Text('Top 250 TVs in the world'),
                leading: Icon(Icons.tv),
                trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.pushReplacementNamed(context, 'tvs')
              ),
            ),
          ],
        ),
      ),

    );
  }
}

