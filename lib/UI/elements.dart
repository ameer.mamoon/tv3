import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchController extends GetxController{
  bool _isSearch = false;

  bool get isSearch => _isSearch;

  static final SearchController controller = Get.put(SearchController());

  void press(){
    _isSearch = ! _isSearch;
    update();
  }
}

class SearchWidget extends StatelessWidget {

  final String title;
  final TextEditingController textController;


  SearchWidget(this.title,this.textController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: SearchController.controller,
      builder: (controller){
        return controller.isSearch ?
            TextField(
              controller: textController,
              decoration: InputDecoration(
                hintText: 'Search',
                icon: IconButton(
                  icon: Icon(Icons.close,color: Colors.black,),
                  onPressed: (){
                    textController.text = '';
                    SearchController.controller.press();
                  },
                )
              ),
            ):
            Text(title);
      },
    );
  }
}

