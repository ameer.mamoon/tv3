
class TvModel{

  final String rank;
  final String title;
  final String fullTitle;
  final String year;
  final String image;
  final String crew;
  final String imDbRating;
  final String imDbRatingCount;

  TvModel(
      {
        required this.rank,
        required this.title,
        required this.fullTitle,
        required this.year,
        required this.image,
        required this.crew,
        required this.imDbRating,
        required this.imDbRatingCount
      }
      );


  factory TvModel.map(Map<String,dynamic> m) =>
      TvModel(
        title: m['title'] ?? 'null',
        image: m['image'] ?? 'null',
        year: m['year'] ?? 'null',
        rank: m['rank'] ?? 'null',
        crew: m['crew'] ?? 'null',
        fullTitle: m['fullTitle'] ?? 'null',
        imDbRating: m['imDbRating'] ?? 'null',
        imDbRatingCount: m['imDbRatingCount'] ?? 'null'
      );

  @override
  String toString() {
    return 'TvModel{rank: $rank, title: $title, fullTitle: $fullTitle, year: $year, image: $image, crew: $crew, imDbRating: $imDbRating, imDbRatingCount: $imDbRatingCount}';
  }

}



class SearchModel{

  final String image ;
  final String title ;
  final String description ;

  SearchModel(
      {
      required this.image,
      required this.title,
      required this.description
      }
      );

  factory SearchModel.map(Map<String,dynamic> m) =>
      SearchModel(
        image: m['image'] ?? 'null',
        title: m['title'] ?? 'null',
        description: m['description'] ?? 'null',
      );

  @override
  String toString() {
    return 'SearchModel{image: $image, title: $title, description: $description}';
  }
}


