import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'models.dart';

class TvController {

  static Future<List<TvModel>> getTop250Movies() async {
    var response = await http.get(
        Uri.parse('https://jsonkeeper.com/b/N1CT'),
    );
    Map<String,dynamic> data = jsonDecode(response.body);

    List<TvModel> models = [];
    List<dynamic> items = data['items'];

    for(var x in items) {
      models.add(
          TvModel.map(x)
      );
    }

    print(models.length);

    return models;
  }

  static Future<List<TvModel>> getTop250TVs() async {
    var response = await http.get(
        Uri.parse('https://jsonkeeper.com/b/VOMM')
    );
    Map<String,dynamic> data = jsonDecode(response.body);

    List<TvModel> models = [];
    List<dynamic> items = data['items'];

    for(var x in items) {
      models.add(
          TvModel.map(x)
      );
    }

    print(models.length);

    return models;
  }

  static Future<List<SearchModel>> search(String value) async{
    var response = await http.get(Uri.parse('https://imdb-api.com/en/API/SearchTitle/k_2k6w9uxj/$value'));
    Map<String,dynamic> data = jsonDecode(response.body);
    List<dynamic> results = data['results'];
    List<SearchModel> models = [];

    for(var x in results)
      models.add(SearchModel.map(x));

    return models;
  }

}