import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tv/API/models.dart';
import 'dart:async';
import 'controllers.dart';

class TvWidget extends StatelessWidget {
  final TvModel model;

  TvWidget(this.model);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double length = min(size.height,size.width);
    return GestureDetector(
      child: Container(
        width: length*0.45,
        height: length*0.7,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(length*0.025)
        ),
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(model.image),
                    fit: BoxFit.fill,
                  ),
                    borderRadius: BorderRadius.vertical(top: Radius.circular(length*0.025))
                ),
              ),
            ),
            ListTile(
              title: Text(model.title),
              subtitle: Row(
                children: [
                  Text('${model.imDbRating}/10'),
                  Icon(Icons.star,color: Colors.amber,)
                ],
              ),
              trailing: Text(model.rank),
            ),
          ],
        ),
        margin: EdgeInsets.all(length*0.025),
      ),
      onTap: ()=> Navigator.push(context,MaterialPageRoute(builder: (context){
        return InfoScreen(model);
      })),
    );
  }


  static Future<List<TvWidget>> getTop250moviesWidgets() async {
    List<TvModel> models = await TvController.getTop250Movies();
    List<TvWidget> widgets = [];

    for(TvModel tv in models){
      widgets.add(TvWidget(tv));
    }

    return widgets;
  }

  static Future<List<TvWidget>> getTop250tvsWidgets() async {
    List<TvModel> models = await TvController.getTop250TVs();
    List<TvWidget> widgets = [];

    for(TvModel tv in models){
      widgets.add(TvWidget(tv));
    }

    return widgets;
  }

}

class InfoScreen extends StatelessWidget {
  final TvModel model;


  InfoScreen(this.model);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(model.title),
        centerTitle: true,
      ),

      body: Center(child: Text(model.toString())),

    );
  }
}

class SearchScreen extends StatelessWidget {

  final String value;


  SearchScreen(this.value);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(value),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: TvController.search(value),
        builder: (context,snapShot){

          if(snapShot.hasData){
            List<SearchModel> models = snapShot.data as List<SearchModel>;
            //Grid View
            return ListView.builder(
                itemCount: models.length,
                itemBuilder: (context,i){
                  return Card(
                    child: ListTile(
                      title: Text(models[i].title),
                      leading: Image.network(models[i].image),
                      subtitle: Text(models[i].description),
                    ),
                  );
                }
            );
          }

          if(snapShot.hasError){
            return Center(
              child: Text('No Internet!'),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}


